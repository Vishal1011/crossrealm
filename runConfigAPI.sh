#!/bin/bash
echo -e  "Add active directory kerberos user domain with bind auth\n"
curl -u btadminuser:P@ssw0rd --request POST http://localhost:8111/PolicyManagement/1.0/user_domains -H "Content-type:application/json" --data '{ "type": "ActiveDirectory", "name": "ADUserDomain", "hostname": "WIN-BPOJ9NP3GPQ.bluetalon.com", "port": "3268", "description": "User Domain For ActiveDirectory", "username": "alice@BLUETALON.COM", "password": "demo#123", "basedn": "DC=bluetalon,DC=com", "groupdiscovery": "true", "btauth": "true", "filter": "cn=*" }'

echo -e "Add user in active directory kerberos user domain\n"
curl -u btadminuser:P@ssw0rd -H "Content-type:application/json" --request PUT http://localhost:8111/PolicyManagement/1.0/user_domains/ADUserDomain/users --data '{ "users": [ ["alice","uid=alice,dc=bluetalon,dc=com"],["eric","uid=eric,dc=bluetalon,dc=com"],["bob","uid=bob,dc=bluetalon,dc=com"],["dan","uid=dan,dc=bluetalon,dc=com"],["charlie","uid=charlie,dc=bluetalon,dc=com"]] }'

echo -e "Add hive kerberos data domain\n"
curl -u btadminuser:P@ssw0rd --header "Content-type:application/json" --request POST http://localhost:8111/PolicyManagement/1.0/resource_domains --data '{  "db_type": "hive","hostname": "172.30.0.116","port": "10000","db_name": "hivedemodb","resource_domain_name": "hivekerbdemo","description": "demo ds of hive with kerberos","login_auth": "true","username": "bob1000@EXAMPLE.COM","password": "test","schema": "hivedemodb","kerberos_principal": "hive/ip-172-30-0-116.ec2.internal@EXAMPLE.COM"}'

echo -e "Add impala kerberos data domain\n"
curl -u btadminuser:P@ssw0rd --header "Content-type:application/json" --request POST http://localhost:8111/PolicyManagement/1.0/resource_domains --data '{  "db_type": "hive","hostname": "172.30.0.116","port": "21050","db_name": "impalademodb","resource_domain_name": "impalakerbdemo","description": "demo ds of impala with kerberos","login_auth": "true","username": "bob1000@EXAMPLE.COM","password": "test","schema": "impalademodb","kerberos_principal": "impala/ip-172-30-0-116.ec2.internal@EXAMPLE.COM"}'

echo -e "Deployment"
curl -u btadminuser:P@ssw0rd  --header "Content-type: application/json" --request PUT http://localhost:8111/PolicyManagement/1.0/deploy --data '{"user":"btadminuser","description":"Cross Realm Test"}'
