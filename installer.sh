RPM_VERSION_INSTALL="3.2.9"
RPM_VERSION_UNINSTALL="3.2.9"
echo $RPM_VERSION_UNINSTALL
sudo yum list installed|grep bluetalon
sudo yum -y remove bluetalon-ep-$RPM_VERSION_UNINSTALL bluetalon-pap-$RPM_VERSION_UNINSTALL bluetalon-pdp-$RPM_VERSION_UNINSTALL bluetalon-policy-$RPM_VERSION_UNINSTALL bt-jre.x86_64
sudo yum -y remove bluetalon-audit-$RPM_VERSION_UNINSTALL bluetalon-audit-aggregate-$RPM_VERSION_UNINSTALL bluetalon-audit-store-psql-$RPM_VERSION_UNINSTALL bluetalon-audit-transform-java-$RPM_VERSION_UNINSTALL bluetalon-audit-visualize-basic-$RPM_VERSION_UNINSTALL bluetalon-ep-$RPM_VERSION_UNINSTALL
sudo yum remove -y bluetalon-*
cd /etc/yum.repos.d/
rm -f BlueTalonLatest*
yum clean all
rm -rf /opt/bluetalon*
rm -rf /var/log/bluetalon*
rm -rf /etc/bluetalon*
rm -rf /opt/bluetalon*
#wget https://s3-us-west-2.amazonaws.com/dev.bluetalon.com/BT/centos6/BUILDS/3.2.2-050418-130/BlueTalonLatest.repo
#wget https://s3-us-west-2.amazonaws.com/dev.bluetalon.com/BT/centos6/BUILDS/3.2.2-052917-39/BlueTalonLatest.repo
wget $1
yum clean all
yum search bluetalon
yum -y install bluetalon-policy-$RPM_VERSION_INSTALL
yum -y install bluetalon-audit-aggregate-$RPM_VERSION_INSTALL bluetalon-audit-store-psql-$RPM_VERSION_INSTALL bluetalon-audit-transform-java-$RPM_VERSION_INSTALL bluetalon-audit-visualize-basic-$RPM_VERSION_INSTALL bluetalon-ep-$RPM_VERSION_INSTALL bluetalon-audit-$RPM_VERSION_INSTALL
/opt/bluetalon/$RPM_VERSION_INSTALL/audit/scripts/bluetalon-audit-setup -accept -silent
/opt/bluetalon/$RPM_VERSION_INSTALL/policy/scripts/bluetalon-policy-setup -accept -silent
echo "Setting up the required libraries..."
sudo cp /root/jdbcjars/* /opt/bluetalon/$RPM_VERSION_INSTALL/policy/pap/webapps/BlueTalonConfig/WEB-INF/lib/
sudo cp /root/jdbcjars/* /opt/bluetalon/$RPM_VERSION_INSTALL/policy/pap/webapps/PolicyManagement/WEB-INF/lib/
echo "Restarting the policy server..."
sudo service bt-policy-server restart
echo "BT Installation completed"
